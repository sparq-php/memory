# Sparq\Memory

[![pipeline status](https://gitlab.com/sparq-php/memory/badges/master/pipeline.svg)](https://gitlab.com/sparq-php/memory/commits/master)
[![Latest Stable Version](https://poser.pugx.org/sparq-php/memory/v/stable)](https://packagist.org/packages/sparq-php/memory)
[![coverage report](https://gitlab.com/sparq-php/memory/badges/master/coverage.svg)](https://gitlab.com/sparq-php/memory/commits/master)
[![Total Downloads](https://poser.pugx.org/sparq-php/memory/downloads)](https://packagist.org/packages/sparq-php/memory)
[![License](https://poser.pugx.org/sparq-php/memory/license)](https://packagist.org/packages/sparq-php/memory)

A simple to use memory usage class.

### Installation and Autoloading

The recommended method of installing is via [Composer](https://getcomposer.org/).

Run the following command from your project root:

```bash
$ composer require sparq-php/memory
```

### Objectives

* Simple to use memory usage class
* Easy integration with other packages/frameworks
* Fast and low footprint

### Usage

```php
require_once __DIR__ . "/vendor/autoload.php";

use Sparq\Memory\Usage;

print_r(Usage::summary());
```