<?php

namespace Sparq\Memory;

/*
 * Memory usage
 */
class Usage
{
    public static $units = ['B', 'KB', 'MB', 'GB', 'TB'];

    /**
     * Memory usage summary.
     *
     * @param bool $real          Real usage
     * @param bool $show_friendly Show friendly
     *
     * @return array Summary
     */
    final public static function summary($real = false, $show_friendly = true)
    {
        /*
         * Result
         */

        $result = [
            'usage' => [
                'bytes' => memory_get_usage($real),
            ],
            'peak_usage' => [
                'bytes' => memory_get_peak_usage($real),
            ],
        ];

        /*
         * Add friendly readable strings
         */

        if (true === $show_friendly) {
            $result['usage']['friendly'] = self::friendly($result['usage']['bytes']);
            $result['peak_usage']['friendly'] = self::friendly($result['peak_usage']['bytes']);
        }

        return $result;
    }

    /**
     * Return friendly description representation of bytes.
     *
     * @param int $bytes Bytes
     *
     * @return string Friendly description
     */
    private static function friendly($bytes)
    {
        $i = floor(log($bytes, 1000));

        return round($bytes / pow(1000, $i), [0, 0, 2, 2, 3][$i]).' '.['B', 'kB', 'MB', 'GB', 'TB'][$i];
    }
}
