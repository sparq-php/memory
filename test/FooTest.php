<?php

namespace Sparq\Memory\Test;

use PHPUnit\Framework\TestCase;
use Sparq\Memory\Usage;

class TestUsage extends TestCase
{
    public function testDefaultReturn()
    {
        $summary = Usage::summary();

        $this->assertInternalType('array', $summary);
        $this->assertArrayHasKey('usage', $summary);
        $this->assertArrayHasKey('bytes', $summary['usage']);
        $this->assertArrayHasKey('friendly', $summary['usage']);
        $this->assertArrayHasKey('peak_usage', $summary);
        $this->assertArrayHasKey('bytes', $summary['peak_usage']);
        $this->assertArrayHasKey('friendly', $summary['peak_usage']);
    }

    public function testDefaultReturnUsage()
    {
        $summary = Usage::summary();

        $this->assertArrayHasKey('bytes', $summary['usage']);
        $this->assertInternalType('int', $summary['usage']['bytes']);
        $this->assertArrayHasKey('friendly', $summary['usage']);
        $this->assertInternalType('string', $summary['usage']['friendly']);
    }

    public function testDefaultReturnPeakUsage()
    {
        $summary = Usage::summary();

        $this->assertArrayHasKey('bytes', $summary['peak_usage']);
        $this->assertInternalType('int', $summary['peak_usage']['bytes']);
        $this->assertArrayHasKey('friendly', $summary['peak_usage']);
        $this->assertInternalType('string', $summary['peak_usage']['friendly']);
    }

    public function testReturnWithOutFriendly()
    {
        $summary = Usage::summary(true, false);

        $this->assertInternalType('array', $summary);
        $this->assertArrayHasKey('usage', $summary);
        $this->assertArrayHasKey('bytes', $summary['usage']);
        $this->assertArrayNotHasKey('friendly', $summary['usage']);
        $this->assertArrayHasKey('peak_usage', $summary);
        $this->assertArrayHasKey('bytes', $summary['peak_usage']);
        $this->assertArrayNotHasKey('friendly', $summary['peak_usage']);
    }
}
